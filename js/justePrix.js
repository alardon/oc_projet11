// Etape 1 - Sélectionner nos éléments
let input =document.querySelector('#prix');
let error = document.querySelector('small');
let formulaire = document.querySelector('#formulaire');

// Etape 2 - Cacher l'erreur
error.style.display = "none";

// Etape 3 - Générer un nombre aléatoire
let nombreAleatoire = Math.floor(Math.random() * 1001);
let coups = 0;
let nombreChoisi = 0;

// Etape 4 - Créer la fonction vérifier
function verifier(nombre) {
    let instruction = document.createElement('div');

    if(nombre < nombreAleatoire) {
        //c'est plus
        instruction.textContent = "#" + coups + " ( " + nombre + " ) C'est plus !";       //ajouter un contenu

        //ajout du css au div en place
        instruction.className = "instruction plus"

    }else if (nombre > nombreAleatoire){
        //c'est moins
        instruction.textContent = "#" + coups + " ( " + nombre + " ) C'est moins !";       //ajouter un contenu

        //ajout du css au div en place
        instruction.className = "instruction moins"

    }else{
        //c'est gagné
        instruction.textContent = "#" + coups + " ( " + nombre + " ) Félicitation !";       //ajouter un contenu

        //ajout du css au div en place
        instruction.className = "instruction fini"
    }

    //Ajouter l'élément devant les autres
    document.querySelector('#instructions').prepend((instruction));

}

// Etape 5 - Vérifier que l'utilisateur donne bien un nombre
input.addEventListener('keyup', () => {
    if(isNaN(input.value)) {
        //Afficher le message d'erreur
        error.style.display = "inline";
    }else{
        //cacher le message d'erreur
        error.style.display = "none";
    }
});

// Etape 6 - Agir à l'envoi du formulaire
formulaire.addEventListener('submit', (e) => {
    e.preventDefault();
    if(isNaN(input.value) || input.value == '' ) {
        //Mettre bordure en rouge
        input.style.borderColor = "red";
    }else{
        //Mettre bordure en rouge
        input.style.borderColor = "silver";
        coups++;
        nombreChoisi = input.value;
        input.value = '';
        verifier(nombreChoisi);
    }
})

