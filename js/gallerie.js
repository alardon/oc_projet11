/*
let openPhotoSwipe = function (){
    let pswpElement = document.querySelectorAll('.pswp')[0];

    let items = [
        {
            src:'https://cdn.pixabay.com/photo/2021/06/17/19/06/sunset-6344387_960_720.jpg',
            w:1440,
            h:1024
        },
        {
            src:'https://cdn.pixabay.com/photo/2021/06/04/15/51/coast-6310250_960_720.jpg',
            w:1440,
            h:1024
        }
    ];

    let options = {
        history:false,
        focus:false,

        showAnimationDuration: 0,
        hideAnimationDuration: 0
    };

    let gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

    gallery.init();
};

/!*openPhotoSwipe();*!/
document.getElementById('btn').onclick = openPhotoSwipe;*/
var openPhotoSwipe = function() {
    var pswpElement = document.querySelectorAll('.pswp')[0];
    // Création d'un array avec toutes les images à afficher
    var items = [
        {
            src: 'https://cdn.pixabay.com/photo/2021/06/17/19/06/sunset-6344387_960_720.jpg', // URL de l'image à afficher
            w: 720,  // Largeur l'image 1 (en px)
            h: 512  // Hauteur l'image 1 (en px)
        },
        {
            src: 'https://cdn.pixabay.com/photo/2021/06/04/15/51/coast-6310250_960_720.jpg', // URL de l'image à afficher
            w: 720, // Largeur l'image 2 (en px)
            h: 512   // Hauteur l'image 2 (en px)
        },
        //...
        {
            src: '/*URL image n */', // URL de l'image à afficher
            w: 1024, // Largeur de l'image n (en px)
            h: 683   // Hauteur de l'image n (en px)
        },
    ];
    // Definition d'autres options plus avancées
    var options = {
        history: false, // Si cette valeur est False, lorsque le retour en arrière est fait, la présentation est quittée.
        focus: false,   // Garde le focus sur l'élément PhotoSwipe si cette valeur est TRUE
        showAnimationDuration: 0,  // Propriété plus complexe, à developper
        hideAnimationDuration: 0   // Propriété plus complexe, à developper
    };
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
};

// Appel de la fonction ouvrant la gallerie photo par un bouton de lancement
openPhotoSwipe();
document.getElementById('btn').onclick = openPhotoSwipe;