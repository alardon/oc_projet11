const url = "https://blockchain.info/ticker";

function recupererPrix() {
//Créer une requête
    let requete = new XMLHttpRequest();

    requete.open('GET', url);//Premier paramètre GET / POST - Deuxième URL
    requete.responseType = 'json'; //choix du format de réponse
    requete.send(); //envoi de la requête

//Dès qu'on reçoit une réponse , cette fonction est executée
    requete.onload = function () {
        if (requete.readyState === XMLHttpRequest.DONE) {
            if (requete.status === 200) {
                let reponse = requete.response; //on stock la réponse
                let prixEnEuros = reponse.EUR.last;
                document.querySelector('#price_label').textContent = prixEnEuros;
            } else {
                alert('Un problème est intervenu, merci de revenir plus tard.')
            }
        }
    }
    console.log("Prix Acutalisé");
}

setInterval(recupererPrix, 500);