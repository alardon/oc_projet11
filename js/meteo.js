let ville = 'Lyon';
recevoirTemperature(ville)

let changerDeVille = document.querySelector('#changer');
changerDeVille.addEventListener('click', () => {
    ville = prompt('Vous souhaitez connaître la température de quelle ville?');
    recevoirTemperature(ville);
})

function recevoirTemperature(ville) {
//Créer une requête
    const url = 'https://api.openweathermap.org/data/2.5/weather?q=' + ville + '&appid=b641a7222bee9528c524006c6d5b42d5&units=metric';

    let requete = new XMLHttpRequest();

    requete.open('GET', url);//Premier paramètre GET / POST - Deuxième URL
    requete.responseType = 'json'; //choix du format de réponse
    requete.send(); //envoi de la requête

//Dès qu'on reçoit une réponse , cette fonction est executée
    requete.onload = function () {
        if (requete.readyState === XMLHttpRequest.DONE) {
            if (requete.status === 200) {
                let reponse = requete.response; //on stock la réponse
                let temperature = reponse.main.temp;
                let ville = reponse.name;

                document.querySelector('#ville').textContent = ville;
                document.querySelector('#temperature_label').textContent = temperature;
            } else {
                alert('Un problème est intervenu, merci de revenir plus tard.')
            }
        }
    }
}